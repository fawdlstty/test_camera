
import 'dart:async';

import 'package:flutter/services.dart';

class TestCamera {
  static const MethodChannel _method_channel = const MethodChannel('test_camera');
  static const EventChannel _event_channel = const EventChannel('image_callback');

  static void init (void Function (dynamic) ondata) {
    _event_channel.receiveBroadcastStream().listen(ondata);
  }

  static Future<String> hello () async {
    final String _ret = await _method_channel.invokeMethod ('hello');
    return _ret;
  }

  static Future<String> start () async {
    final String _ret = await _method_channel.invokeMethod ('start');
    return _ret;
  }

  static Future<String> stop () async {
    final String _ret = await _method_channel.invokeMethod ('stop');
    return _ret;
  }
}
