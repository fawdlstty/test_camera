﻿#pragma warning (disable: 4458)
#pragma warning (disable: 4819)

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#define _CRT_SECURE_NO_WARNINGS
#define _SILENCE_CXX17_ALLOCATOR_VOID_DEPRECATION_WARNING
#define _SILENCE_ALL_CXX17_DEPRECATION_WARNINGS
#define _WIN32_WINNT _WIN32_WINNT_WIN7
#include <sdkddkver.h>
#include <windows.h>
#include <scesvc.h>
#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#include <gdiplus.h>
#undef min
#undef max
#pragma comment (lib, "Gdiplus.lib")

#include "include/test_camera/test_camera_plugin.h"

#include <VersionHelpers.h>
#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>
#include <flutter/standard_method_codec.h>
#include <flutter/event_channel.h>

#include <atomic>
#include <chrono>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <mutex>
#include <sstream>
#include <thread>
#include <vector>

//#include "D:/GitHub/vcpkg/installed/x64-windows-static/include/fmt/core.h"



class MyException : public std::exception {
public:
};

template<typename T = flutter::EncodableValue>
class MyStreamHandler: public flutter::StreamHandler<T> {
public:
	MyStreamHandler () = default;
	virtual ~MyStreamHandler () = default;

	void on_callback (uint8_t *_data, size_t _size) {
        if (uint64_t (this) == 0xddddddddddddddddul)
            return;
        std::unique_lock<std::mutex> _ul (m_mtx);
		std::vector<uint8_t> _vec;
		_vec.resize (_size);
		memcpy (&_vec [0], _data, _size);
		m_value = _vec;
        if (m_sink.get ())
		    m_sink.get ()->Success (m_value);
	}

protected:
	std::unique_ptr<flutter::StreamHandlerError<T>> OnListenInternal (const T *arguments, std::unique_ptr<flutter::EventSink<T>> &&events) override {
        std::unique_lock<std::mutex> _ul (m_mtx);
		m_sink = std::move (events);
        return nullptr;
	}

	std::unique_ptr<flutter::StreamHandlerError<T>> OnCancelInternal (const T *arguments) override {
        std::unique_lock<std::mutex> _ul (m_mtx);
		m_sink.release ();
        return nullptr;
	}

private:
	flutter::EncodableValue m_value;
    std::mutex m_mtx;
	std::unique_ptr<flutter::EventSink<T>> m_sink;
};

class TestCameraPlugin : public flutter::Plugin {
    std::unique_ptr<flutter::MethodChannel<flutter::EncodableValue>> m_method_channel;
    std::unique_ptr<flutter::EventChannel<flutter::EncodableValue>> m_event_channel;

public:
    static void RegisterWithRegistrar (flutter::PluginRegistrarWindows *registrar) {
        auto _plugin = std::make_unique<TestCameraPlugin> ();
        _plugin->m_run.store (false);
        //
        _plugin->m_method_channel = std::make_unique<flutter::MethodChannel<flutter::EncodableValue>> (
            registrar->messenger (), "test_camera",
            &flutter::StandardMethodCodec::GetInstance ()
        );
        _plugin->m_method_channel->SetMethodCallHandler ([_plugin_pointer = _plugin.get ()] (const auto &call, auto result) {
            _plugin_pointer->HandleMethodCall (call, std::move (result));
        });
        //
        _plugin->m_event_channel = std::make_unique<flutter::EventChannel<flutter::EncodableValue>> (
           registrar->messenger (), "image_callback",
           &flutter::StandardMethodCodec::GetInstance ()
        );
        _plugin->m_handler = new MyStreamHandler<> ();
        auto _obj_stm_handle = static_cast<flutter::StreamHandler<flutter::EncodableValue>*> (_plugin->m_handler);
        std::unique_ptr<flutter::StreamHandler<flutter::EncodableValue>> _ptr {_obj_stm_handle};
        _plugin->m_event_channel->SetStreamHandler (std::move (_ptr));
        //
        registrar->AddPlugin (std::move (_plugin));
    }
    TestCameraPlugin () {}
    virtual ~TestCameraPlugin () {}

private:
    void HandleMethodCall (const flutter::MethodCall<flutter::EncodableValue> &method_call, std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result) {
        std::string _method = method_call.method_name ();
        if (_method == "hello") {
            //::MessageBoxA (NULL, "on hello", "tips", MB_OK);
            result->Success (flutter::EncodableValue ("hello world"));
        } else if (_method == "start") {
            //::MessageBoxA (NULL, "on start", "tips", MB_OK);
            if (!m_run.load ()) {
                m_run.store (true);
                std::thread ([this] () {
                    size_t _cur = 0;
                    auto _t = std::chrono::system_clock::now ();
                    while (m_run.load ()) {
                        std::string &_img_buf = s_img_bufs [_cur % s_img_bufs.size ()];
                        m_handler->on_callback ((uint8_t*) _img_buf.data (), _img_buf.size ());
                        std::this_thread::sleep_until (_t + std::chrono::milliseconds (1000 * _cur / s_img_bufs.size ()));
                        ++_cur;
                    }
                }).detach ();
            }
            result->Success (flutter::EncodableValue ("a"));
        } else if (_method == "stop") {
            m_run.store (false);
            result->Success (flutter::EncodableValue ("b"));
        } else {
            result->NotImplemented ();
        }
    }

public:
	MyStreamHandler<> *m_handler = nullptr;
    std::atomic<bool> m_run;
    static std::vector<std::string> s_img_bufs;
};

std::vector<std::string> TestCameraPlugin::s_img_bufs;

std::wstring gb18030_to_utf16 (std::string _old) {
    int lenOld = lstrlenA (_old.data ());
    int lenNew = ::MultiByteToWideChar (CP_ACP, 0, _old.data (), lenOld, nullptr, 0);
    std::wstring s;
    s.resize (lenNew);
    if (!::MultiByteToWideChar (CP_ACP, 0, _old.data (), lenOld, const_cast<wchar_t*>(s.c_str ()), lenNew))
        return L"";
    return s.c_str ();
}

void TestCameraPluginRegisterWithRegistrar (FlutterDesktopPluginRegistrarRef registrar) {
    static bool s_init = true;
    if (s_init) {
        s_init = false;
        // ULONG_PTR _token = 0;
        // Gdiplus::GdiplusStartupInput _input;
        // Gdiplus::GdiplusStartupOutput _output;
        // Gdiplus::GdiplusStartup (&_token, &_input, &_output);
        // // 读文件
        // for (int i = 0; i < 16; ++i) {
        //     // D:\Desktop\screenhosts\0~15.png
        //     std::string _path = fmt::format ("D:\\Desktop\\screenhosts\\{}.png", i);
        //     std::wstring _wpath = gb18030_to_utf16 (_path);
        //     Gdiplus::Bitmap *_img = Gdiplus::Bitmap::FromFile (_wpath.data ());
        //     uint8_t *_img_buf = new uint8_t [640 * 480 * 3];
        //     Gdiplus::BitmapData _data;
        //     Gdiplus::Rect _rc { 0, 0, 640, 480 };
		// 	_img->LockBits (&_rc, Gdiplus::ImageLockModeRead, PixelFormat24bppRGB, &_data);
		// 	memcpy (_img_buf, _data.Scan0, 640 * 480 * 3);
		// 	_img->UnlockBits (&_data);
        //     delete _img;
        //     TestCameraPlugin::s_img_bufs.push_back (_img_buf);
        // }
        char _xpath [260];
        GetModuleFileNameA (NULL, _xpath, 260);
        std::string _spath = _xpath;
        size_t _p = _spath.find ("\\example\\build\\");
        _spath.resize (_p);
        _spath += "\\screenhosts\\";
        //MessageBoxA (NULL, _spath.data (), "hello", MB_ICONHAND);
        for (int i = 0; i < 16; ++i) {
            //std::string _path = fmt::format ("D:\\Desktop\\screenhosts\\{}.png", i);
            std::stringstream _ss;
            _ss << _spath << i << ".png";
            std::string _path = "";
            _ss >> _path;
            std::ifstream _ifs (_path, std::ios::binary);
            _ifs.seekg (0, std::ios_base::end);
            std::streampos _flen = _ifs.tellg ();
            std::string _sbuf;
            _sbuf.resize (_flen);
            _ifs.seekg (0, std::ios_base::beg);
            //std::string _sbuf ((std::istreambuf_iterator<char> (_ifs)), std::istreambuf_iterator<char> ());
            _ifs.read (_sbuf.data (), _flen);
            _ifs.close ();
            //std::string _show = fmt::format ("path[{}], size[{}], filesize[{}]", _path.data (), _sbuf.size (), _flen);
            //MessageBoxA (NULL, _show.data (), "hello", MB_ICONHAND);
            TestCameraPlugin::s_img_bufs.push_back (_sbuf);
        }
    }
    //TestCameraPlugin::s_img_bufs.push_back (new uint8_t [640 * 480 * 3]);
    TestCameraPlugin::RegisterWithRegistrar (
        flutter::PluginRegistrarManager::GetInstance ()->GetRegistrar<flutter::PluginRegistrarWindows> (registrar)
    );
}
