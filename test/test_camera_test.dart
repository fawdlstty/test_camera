import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:test_camera/test_camera.dart';

void main() {
  const MethodChannel channel = MethodChannel('test_camera');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await TestCamera.platformVersion, '42');
  });
}
