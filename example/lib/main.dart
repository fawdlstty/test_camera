import 'dart:developer';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:test_camera/test_camera.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _title = 'a';
  Image _image;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    TestCamera.init((bytes) {
      //Uint8List _bytes = bytes;
      //print (_bytes.length);
      //_bytes.clear();
      Image img = Image.memory(bytes);
      setState(() {
        //_image = img;
        print ("on callback");
      });
    });

    String _t = await TestCamera.hello();
    setState(() {
      _title = _t;
    });

    await TestCamera.start();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(_title),
        ),
        body: Center(
          child: _image,
        ),
      ),
    );
  }
}
