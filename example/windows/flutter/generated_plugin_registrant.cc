//
//  Generated file. Do not edit.
//

#include "generated_plugin_registrant.h"

#include <test_camera/test_camera_plugin.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  TestCameraPluginRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("TestCameraPlugin"));
}
